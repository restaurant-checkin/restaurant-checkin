@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row py-5 justify-content-center">
            <div class="col-md-7 col-sm-12">
                <h4 class="display-4 text-center mb-3">Login as Crew</h4>
                <div class="card card-body mb-3" style="border-top: 12px solid gray">
                    <h4>Restaurant Check In</h4>
                    <p>
                        Starting from September 15, PEMDA DKI has decided to open its doors and welcome you back to our exhibition space—with precautionary conditions.
                        <br><br>
                        In order to mitigate the further spread of the COVID-19, we have prepared a new set of procedures that should be followed by anyone who sets foot in our space.
                        <br><br>
                        Before visiting us, please fill in this registration form. Our door will be locked at all times and our exhibition guide will open the door for you based on the time of visit you choose in the form. You may also fill the form at the time you arrive at our compound.
                        <br><br>
                        One form is only valid for 1 person. So, if you come in groups, each person will have to individually fill the form.
                        <br><br>
                        After you fill this form, you will receive a confirmation email on the morning of your chosen date of visit.
                        <br><br>
                        <b class="text-danger">* Required</b>
                    </p>
                </div>
                <form class="needs-validation" novalidate="" id="data-form" action="{{url('/login')}}" method="post">
                    @if(session('status'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Error !</strong> {{session('status')}}.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    {{csrf_field()}}
                    <div class="mb-3 card card-body">
                        <label for="email">Email Address <sup class="text-danger"><b>*</b></sup></label>
                        <div class="w-50">
                            <input type="email" class="form-control border-left-0 border-top-0 border-right-0 rounded-0 p-0" name="email" id="email" required placeholder="Your Email">
                            <div class="invalid-feedback">
                                Please enter a valid email address.
                            </div>
                        </div>
                    </div>

                    <div class="mb-3 card card-body">
                        <label for="password">Password <sup class="text-danger"><b>*</b></sup></label>
                        <div class="w-50">
                            <input type="password" class="form-control border-left-0 border-top-0 border-right-0 rounded-0 p-0" name="password" id="password" placeholder="Your Password" required>
                            <div class="invalid-feedback">
                                Please enter your password.
                            </div>
                        </div>
                    </div>

                    <hr class="mb-4">
                    <button class="btn btn-secondary" type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        document.addEventListener('DOMContentLoaded', function(e) {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })
    </script>
@endsection
