<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rucigang - Restaurant Checkin</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>
    <h4 class="display-4">Rucigang - Restaurant Checkin</h4>
    <p>Terima kasih sudah mendaftarkan data diri anda di aplikasi Rucigang ini, berikut Link QR Code yang kami berikan: </p>
    <a href="{{$url_code}}">Link QR Code</a>
    <p><b>Note</b> : Silahkan perlihatkan Link QR Code ini ke petugas restaurant untuk dilakukan Pendataan data diri anda.</p>
    <p>Terimakasih</p>
</body>
</html>
