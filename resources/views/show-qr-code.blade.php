@extends('layouts.app')

@section('content')
    <div class="row py-5 justify-content-center">
        <div class="col-md-7 col-sm-12">
            <div class="card card-body mb-3">
                <div class="visible-print text-center">
                    <div id="qr-code" class="d-inline-block"></div>
                    <h4 class="my-3">Mohon di tunjukan ke Petugas</h4>
                    <small>Link QR Code berhasil dikirim ke email anda.</small><br>
                    <canvas id="canvas" class="d-none" width="200" height="200"></canvas>
                    <button class="btn btn-outline-secondary" onclick="downloadQR(this)">Save QR Code</button>
                </div>
            </div>
            <div class="card card-body mb-3">
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label">Email</label>
                    <div class="col-sm-8">
                        <input type="text" readonly class="form-control" id="staticEmail" value="{{$customer->email}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="fullname" class="col-sm-4 col-form-label">Full-name</label>
                    <div class="col-sm-8">
                        <input type="text" readonly class="form-control" id="fullname" value="{{$customer->name}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="ig_username" class="col-sm-4 col-form-label">Instagram Username</label>
                    <div class="col-sm-8">
                        <input type="text" readonly class="form-control" id="ig_username" value="{{$customer->ig_username}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone-number" class="col-sm-4 col-form-label">Phone Number</label>
                    <div class="col-sm-8">
                        <input type="text" readonly class="form-control" id="phone-number" value="{{$customer->mobile_num}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-sm-4 col-form-label">Address</label>
                    <div class="col-sm-8">
                        <input type="text" readonly class="form-control" id="address" value="{{$customer->address}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('style')
    <style>
        .card{
            border-radius: 10px;
        }
    </style>
@endsection

@section('script')
    <script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
    <script src="{{asset('js/saveSvgAsPng.js')}}"></script>
    <script src="{{asset('js/easy.qrcode.min.js')}}" type="text/javascript" charset="utf-8"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function(e) {
          setInputFilter(document.getElementById("phone-number"), function(value) {
                return /^\d*$/.test(value); });

            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });


            var optionQRCode = {
                text: '{{$customer->url_code}}/checkin',
                width: 200,
                height: 200,
                colorDark : "#000000",
                colorLight : "#ffffff",
                correctLevel : QRCode.CorrectLevel.L,
                dotScale: 1,
            };

            var qrcode = new QRCode("qr-code", optionQRCode);
        })

        function handlerIsOtherTransportation(e) {
            let isChecked = e.checked
            if(isChecked === true) {
                document.getElementsByName('other-transportation-text')[0].disabled = false
            }else{
                document.getElementsByName('other-transportation-text')[0].disabled = true
                document.getElementsByName('other-transportation-text')[0].value = ''
                document.getElementById('other-transportation').value = ''
            }
        }

        function handlerIsOtherTravel(e) {
            if(e.value === 'other') {
                document.getElementsByName('other-travel-text')[0].disabled = false
            }else{
                document.getElementsByName('other-travel-text')[0].disabled = true
                document.getElementsByName('other-travel-text')[0].value = ''
            }
        }

        function otherTransportation(e){
            document.getElementById('other-transportation').value = e.value
        }

        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    } else {
                        this.value = "";
                    }
                });
            });
        }

        function downloadQR(e) {
            var canvas = $('canvas')[0];
            var img = document.querySelector('img');
            var ctx = canvas.getContext('2d');
            ctx.fillStyle = '#FFFFFF';
            ctx.fillRect(0,0,250,250);
            ctx.drawImage(img, 10,10,180,180);
            imgData = canvas.toDataURL();
            imgDatajpeg = canvas.toDataURL("image/jpeg");
            console.log(imgDatajpeg);
            var a = document.createElement('a');
            a.href = imgDatajpeg;
            a.download = 'qrcode.png';
            a.click();
        }
    </script>
@endsection
