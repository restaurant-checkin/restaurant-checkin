@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row py-5 justify-content-center">
            <div class="col-md-7 col-sm-12">
                <form class="needs-validation" novalidate="" action="{{url($customer->url_code.'/checkin')}}" method="post">
                    {{csrf_field()}}
                    <div class="card card-body mb-3">
                        <h4 class="display-4 text-center mb-3">Check In</h4>
                        <div class="form-group row">
                            <label for="fullname" class="col-sm-4 col-form-label">Full-name</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control" id="fullname" value="{{$customer->name}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phone-number" class="col-sm-4 col-form-label">Phone Number</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control" id="phone-number" value="{{$customer->mobile_num}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address" class="col-sm-4 col-form-label">Address</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control" id="address" value="{{$customer->address}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="destination" class="col-sm-4 col-form-label">Destination</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control" id="destination" value="{{$customer->destination}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="destination" class="col-sm-4 col-form-label">Date of Visit</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control" id="destination" value="{{$customer->destination}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="destination" class="col-sm-4 col-form-label">Time of Visit</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control" id="destination" value="{{$customer->destination}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="body-temperature" class="col-sm-4 col-form-label">Body Temperature</label>
                            <div class="col-sm-8">
                                <div class="range-slider">
                                    <input class="range-slider__range" name="body-temperature" id="body-temperature" type="range" value="36" min=36 max=40 step=.1>
                                    <span class="range-slider__value">36</span>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-secondary" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('style')
    <style>
        .card {
            border-radius: 10px;
        }
        .range-slider {
            margin: 0px 0 0 0%;
        }

        .range-slider {
            width: 100%;
        }

        .range-slider__range {
            -webkit-appearance: none;
            width: calc(100% - (73px));
            height: 10px;
            border-radius: 5px;
            background: #d7dcdf;
            outline: none;
            padding: 0;
            margin: 0;
        }
        .range-slider__range::-webkit-slider-thumb {
            -webkit-appearance: none;
            appearance: none;
            width: 20px;
            height: 20px;
            border-radius: 50%;
            background: #2c3e50;
            cursor: pointer;
            -webkit-transition: background .15s ease-in-out;
            transition: background .15s ease-in-out;
        }
        .range-slider__range::-webkit-slider-thumb:hover {
            background: #1abc9c;
        }
        .range-slider__range:active::-webkit-slider-thumb {
            background: #1abc9c;
        }
        .range-slider__range::-moz-range-thumb {
            width: 20px;
            height: 20px;
            border: 0;
            border-radius: 50%;
            background: #2c3e50;
            cursor: pointer;
            -moz-transition: background .15s ease-in-out;
            transition: background .15s ease-in-out;
        }
        .range-slider__range::-moz-range-thumb:hover {
            background: #1abc9c;
        }
        .range-slider__range:active::-moz-range-thumb {
            background: #1abc9c;
        }
        .range-slider__range:focus::-webkit-slider-thumb {
            box-shadow: 0 0 0 3px #fff, 0 0 0 6px #1abc9c;
        }

        .range-slider__value {
            display: inline-block;
            position: relative;
            width: 60px;
            color: #fff;
            line-height: 20px;
            text-align: center;
            border-radius: 3px;
            background: #2c3e50;
            padding: 5px 10px;
            margin-left: 8px;
        }
        .range-slider__value:after {
            position: absolute;
            top: 8px;
            left: -7px;
            width: 0;
            height: 0;
            border-top: 7px solid transparent;
            border-right: 7px solid #2c3e50;
            border-bottom: 7px solid transparent;
            content: '';
        }

        ::-moz-range-track {
            background: #d7dcdf;
            border: 0;
        }

        input::-moz-focus-inner,
        input::-moz-focus-outer {
            border: 0;
        }
    </style>
@endsection

@section('script')
    <script>
        document.addEventListener('DOMContentLoaded', function(e) {
            rangeSlider();
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })
        var rangeSlider = function(){
            var slider = $('.range-slider'),
                range = $('.range-slider__range'),
                value = $('.range-slider__value');

            slider.each(function(){

                value.each(function(){
                    var value = $(this).prev().attr('value');
                    $(this).html(value);
                });

                range.on('input', function(){
                    $(this).next(value).html(this.value);
                });
            });
        };

    </script>
@endsection
