@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row py-5 justify-content-center">
            <div class="col-md-7 col-sm-12">
                <div class="heading d-flex justify-content-around align-items-center">
                    <img src="{{asset("/img/Ruci(1).png")}}" alt="Ruci's Joint Identity" class="img-fluid" id="ruci-img-logo" width="100" height="50">
                    <h4 class="display-4 text-center mb-3">
                        Restaurant <br> Check In
                    </h4>
                    <img src="{{asset("/img/Stiker_Kaca_Rara_page.png")}}" alt="Rara" class="img-fluid" id="ruci-img-logo" width="100" height="50">
                </div>


                <div class="card card-body mb-3 position-relative" style="border-top: 12px solid gray">
                    <h4>Visitor Registration Form</h4>
                    <p>
                        Starting from September 15, PEMDA DKI has decided to open its doors and welcome you back to our exhibition space—with precautionary conditions.
                        <br><br>
                        In order to mitigate the further spread of the COVID-19, we have prepared a new set of procedures that should be followed by anyone who sets foot in our space.
                        <br><br>
                        Before visiting us, please fill in this registration form. Our door will be locked at all times and our exhibition guide will open the door for you based on the time of visit you choose in the form. You may also fill the form at the time you arrive at our compound.
                        <br><br>
                        One form is only valid for 1 person. So, if you come in groups, each person will have to individually fill the form.
                        <br><br>
                        After you fill this form, you will receive a confirmation email on the morning of your chosen date of visit.
                        <br><br>
                        <b class="text-danger">* Required</b>
                    </p>
                </div>
                <form class="needs-validation" novalidate="" id="data-form" action="{{url('/store')}}" method="post">
                    @error('email')
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Error !</strong> {{$message}}.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @enderror
                    {{csrf_field()}}
                    <div class="mb-3 card card-body">
                        <label for="email">Email Address <sup class="text-danger"><b>*</b></sup></label>
                        <div class="w-50">
                            <input type="email" class="form-control border-left-0 border-top-0 border-right-0 rounded-0 p-0" name="email" id="email" required placeholder="Your Email">
                            <div class="invalid-feedback">
                                Please enter a valid email address.
                            </div>
                        </div>
                    </div>

                    <div class="mb-3 card card-body">
                        <label for="fullname">Full-name <sup class="text-danger"><b>*</b></sup></label>
                        <div class="w-50">
                            <input type="text" class="form-control border-left-0 border-top-0 border-right-0 rounded-0 p-0" name="fullname" id="fullname" placeholder="Your answer" required>
                            <div class="invalid-feedback">
                                Please enter your fullname.
                            </div>
                        </div>
                    </div>

                    <div class="mb-3 card card-body">
                        <label for="ig_username">Instagram Username <sup class="text-danger"><b>*</b></sup></label>
                        <div class="w-50">
                            <input type="text" class="form-control border-left-0 border-top-0 border-right-0 rounded-0 p-0" name="ig_username" id="ig_username" placeholder="Your answer" required>
                            <div class="invalid-feedback">
                                Please enter your Username Instagram.
                            </div>
                        </div>
                    </div>

                    <div class="mb-3 card card-body">
                        <label for="area">Area, district that you live in. (Kelurahan, Kecamatan; e.g. Menteng, Gondangdia.)<sup class="text-danger"><b>*</b></sup></label>
                        <div class="w-50">
                            <input type="text" class="form-control border-left-0 border-top-0 border-right-0 rounded-0 p-0" name="area" id="area" required placeholder="Your answer">
                            <div class="invalid-feedback">
                                Please enter your Area / District live in.
                            </div>
                        </div>
                    </div>

                    <div class="mb-3 card card-body">
                        <label for="phone-number">Phone Number<sup class="text-danger"><b>*</b></sup></label>
                        <div class="w-50">
                            <input type="text" class="form-control border-left-0 border-top-0 border-right-0 rounded-0 p-0" name="phone" id="phone-number" required placeholder="Your answer">
                            <div class="invalid-feedback">
                                Please enter your Phone Number.
                            </div>
                        </div>
                    </div>
                    <div class="mb-3 card card-body">
                        <div>
                            <p>What kind of transportation will you use to reach our space? (Can be more than one.) <sup class="text-danger"><b>*</b></sup></p>
                        </div>
                        <div>
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="transportation[]" id="private-transportation" value="Private transportation">
                                <label class="custom-control-label" for="private-transportation">Private Transportation (e.g Car, Motorbike)</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="transportation[]" id="transjakarta-transportation" value="Transjakarta / Jaklingko , etc">
                                <label class="custom-control-label" for="transjakarta-transportation">Transjakarta/Jaklingko etc</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="transportation[]" id="mrt-transportation" value="MRT">
                                <label class="custom-control-label" for="mrt-transportation">MRT</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="transportation[]" id="krl-transportation" value="KRL">
                                <label class="custom-control-label" for="krl-transportation">KRL</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="transportation[]" id="online-transportation" value="Online Taxi">
                                <label class="custom-control-label" for="online-transportation">Online Taxi ( Car / Motorbike )</label>
                            </div>
                            <div class="custom-control custom-checkbox d-flex">
                                <input type="checkbox" class="custom-control-input" name="transportation[]" id="other-transportation" value="" onchange="handlerIsOtherTransportation(this)">
                                <label class="custom-control-label" for="other-transportation">Other</label>
                                <input type="text" name="other-transportation-text" onkeyup="otherTransportation(this)" disabled class="form-control form-control-sm ml-2 border-left-0 border-right-0 border-top-0 rounded-0">
                            </div>
                        </div>
                    </div>

                    <div class="mb-3 card card-body">
                        <div>
                            <p>In the last 14 days, have you visited other cities other than Jakarta? If yes, please tell us which cities in the "other" section <sup class="text-danger"><b>*</b></sup></p>
                        </div>
                        <div>
                            <div class="custom-control custom-radio mb-2">
                                <input id="travel-yes" name="travel" type="radio" class="custom-control-input" value="yes" required>
                                <label class="custom-control-label" for="travel-yes">Yes</label>
                            </div>
                            <div class="custom-control custom-radio mb-2">
                                <input id="travel-no" name="travel" type="radio" class="custom-control-input" value="no" required>
                                <label class="custom-control-label" for="travel-no">No</label>
                            </div>
                            <div class="custom-control custom-radio mb-2 d-flex">
                                <input id="travel-other" name="travel" type="radio" class="custom-control-input" value="other" required>
                                <label class="custom-control-label" for="travel-other">Other</label>
                                <input type="text" name="other-travel-text" disabled class="form-control form-control-sm ml-2 border-left-0 border-right-0 border-top-0 rounded-0">
                            </div>
                        </div>
                    </div>
                    <hr class="mb-4">
                    <button class="btn btn-secondary" type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('style')
    <style>
        .card{
            border-radius: 10px;
        }
        @media only screen and (max-width: 600px) {
            .heading h4{
                font-size: 2rem;
                /*font-weight: bold;*/
            }
            .heading img {
                width: 50px;
                /*height: 35px;*/
            }
        }

    </style>
@endsection

@section('script')
    <script>
        document.addEventListener('DOMContentLoaded', function(e) {
            let form_travel = document.forms[0].travel
            for(let i = 0 ;i < form_travel.length; i++) {
                form_travel[i].onclick = function() {
                    handlerIsOtherTravel(this)
                }
            }

            setInputFilter(document.getElementById("phone-number"), function(value) {
                return /^\d*$/.test(value); });

            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })

        function handlerIsOtherTransportation(e) {
            let isChecked = e.checked
            if(isChecked === true) {
                document.getElementsByName('other-transportation-text')[0].disabled = false
            }else{
                document.getElementsByName('other-transportation-text')[0].disabled = true
                document.getElementsByName('other-transportation-text')[0].value = ''
                document.getElementById('other-transportation').value = ''
            }
        }

        function handlerIsOtherTravel(e) {
            if(e.value === 'other') {
                document.getElementsByName('other-travel-text')[0].disabled = false
            }else{
                document.getElementsByName('other-travel-text')[0].disabled = true
                document.getElementsByName('other-travel-text')[0].value = ''
            }
        }

        function otherTransportation(e){
            document.getElementById('other-transportation').value = e.value
        }

        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    } else {
                        this.value = "";
                    }
                });
            });
        }
    </script>
@endsection
