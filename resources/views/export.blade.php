@extends('layouts.app')
@section('content')
    <div class="row py-5 justify-content-center">
        <div class="col-sm-12">
            <div class="card card-body rounded" style="border-top: 12px solid gray">
                <div class="d-flex justify-content-between justify-align-center">
                    <div class="form-group col-md-2 pl-0">
                        <label for="" class=" col-form-label">Date :</label>
                        <div class="">
                            <select name="" class="form-control form-control-sm" onchange="loadData()" id="filter_date">
                                <option value="2-weeks">Past 2 Weeks</option>
                                <option value="1-month">Past 1 Month</option>
                                <option value="3-month">Past 3 Month</option>
                                <option value="6-month">Past 6 Month</option>
                                <option value="1-year">Past 1 Year</option>
                            </select>
                        </div>
                    </div>
                    <div class="mb-3"><a href="{{url('/logout')}}" class="btn btn-sm btn-outline-dark">Keluar</a></div>
                </div>
                <div class="table-responsive">
                    <table id="export-table" class="table table-sm table table-striped display table-bordered nowrap" cellspacing="0" style="width: 100%">
                        <thead style="font-size: 12px">
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>IG Username</th>
                            <th class="truncate">Mobile Number</th>
                            <th class="truncate">Address (District)</th>
                            <th>Transportation</th>
                            <th class="truncate">Visit Another City</th>
                            <th>Destination</th>
                            <th>Temperature</th>
                            <th class="truncate">Date Visit</th>
                            <th class="truncate">Time Visit</th>
                            <th>Crew</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.21/r-2.2.5/datatables.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    <style>
        tr td {
            font-size: 14px;
        }
        /*.truncate {*/
        /*    max-width:80px;*/
        /*    white-space: nowrap;*/
        /*    overflow: hidden;*/
        /*    text-overflow: ellipsis;*/
        /*}*/
        .pace {
            -webkit-pointer-events: none;
            pointer-events: none;

            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        .pace-inactive {
            display: none;
        }

        .pace .pace-progress {
            background: #000000;
            position: fixed;
            z-index: 2000;
            top: 0;
            right: 100%;
            width: 100%;
            height: 2px;
        }

    </style>
@endsection

@section('script')
    <script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
    <script data-pace-options='{ "ajax": true , "document" : true, "eventLag" : true}' src="{{asset('/js/pace.min.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.21/r-2.2.5/datatables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.21/dataRender/ellipsis.js"></script>
    <script>
        var table = null;
        $(function(){
            table = $('#export-table').removeAttr('width').DataTable({
                dom: 'Bfrtip',
                scrollY:        "300px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
                buttons: [
                    {
                        extend: "excelHtml5",
                        exportOptions: { orthogonal: "exportxls" },
                        text: '<i class="fas fa-file-excel" aria-hidden="true"> Export as excel</i>'
                    },
                    'pageLength'
                ],
                columns: [
                    { data: 'customer_name', name: 'customer_name',
                        render: (data, type, row, index) => {
                            if(type !== 'display' || data.length <= 15) {
                                return data
                            }else{
                                return '<span class="truncate" title="'+data+'">'+data.substr(0,15) +'...</span>'
                            }
                        }
                    },
                    { data: 'customer_email', name: 'customer_email',
                        render: (data, type, row, index) => {
                            if(type !== 'display' || data.length <= 15) {
                                return data
                            }else{
                                return '<span class="truncate" title="'+data+'">'+data.substr(0,15) +'...</span>'
                            }
                        }
                    },
                    { data: 'customer_ig_username', name: 'customer_ig_username',
                        render: (data, type, row, index) => {
                            if(type !== 'display' || data.length <= 15) {
                                return data
                            }else{
                                return '<span class="truncate" title="'+data+'">'+data.substr(0,15) +'...</span>'
                            }
                        }
                    },
                    { data: 'customer_mobile_num', name: 'customer_mobile_num',},
                    { data: 'customer_address', name: 'customer_address',
                        render: (data, type, row, index) => {
                            if(type !== 'display' || data.length <= 33) {
                                return data
                            }else{
                                return '<span class="truncate" title="'+data+'">'+data.substr(0,33) +'...</span>'
                            }
                        }
                    },
                    { data: 'customer_transportation', name: 'customer_transportation',
                        render: (data, type, row, index) => {
                            if(type !== 'display' || data.length <= 15) {
                                return data
                            }else{
                                return '<span class="truncate" title="'+data+'">'+data.substr(0,15) +'...</span>'
                            }
                        }
                    },
                    { data: 'customer_another_city', name: 'customer_another_city', className:"text-center",  render : (data, row, type) => { return data.charAt(0).toUpperCase() + data.slice(1)}},
                    { data: 'users_name_of_place', name: 'destination_place',
                        render: (data, type, row, index) => {
                            if(type !== 'display' || data.length <= 33) {
                                return data
                            }else{
                                return '<span class="truncate" title="'+data+'">'+data.substr(0,33) +'...</span>'
                            }
                        }
                    },
                    { data: 'body_temperature', name: 'body_temperature', className:"text-center", render : (data, row, type) => {
                        if(data <= 36.5) {
                            return '<span class="badge badge-primary">'+data+'</span>'
                        }else if(data > 36.5 && data <= 37.5) {
                            return '<span class="badge badge-warning">'+data+'</span>'
                        }else{
                            return '<span class="badge badge-danger">'+data+'</span>'
                        }
                    }},
                    { data: 'datevisit', name: 'datevisit',},
                    { data: 'timevisit', name: 'timevisit',},
                    { data: 'users_name', name: 'users_name',},
                ],
            });

            table.buttons().container()
                .appendTo('#export-table .col-md-6:eq(0)');

            var filter_date = '<select id="filter_date">' +
                '<option value="2-weeks">past 2 weeks</option>' +
                '<option value="1-month">past 1 month</option>' +
                '<option value="3-month">past 3 month</option>' +
                '<option value="6-month">past 6 month</option>' +
                '<option value="1-year">past 1 year</option></select>';

            $('#filter_date').on('change', function (e) {
                table.draw();
                e.preventDefault();
            })

            loadData();
        });

        function loadData(){
            var response = null;
            $.ajax({
                url : '{{url('/export/json')}}',
                method : 'post',
                data : {
                    _token : '{{csrf_token()}}',
                    filter_date : $('#filter_date').val(),
                },
                async : false,
                success: (res) => {
                    response = res
                },
                error: (error) => {

                }
            });
            table.clear();
            table.rows.add(response);
            table.draw();
        }

    </script>
@endsection
