@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row py-5 justify-content-center">
            <div class="col-md-7 col-sm-12">
                <div class="card card-body jumbotron jumbotron-fluid rounded" style="border-top: 12px solid gray">
                    <div class="position-absolute" style="top: 20px; right: 30px;"><a href="{{url('/logout')}}" class="btn btn-sm btn-outline-dark">Keluar</a></div>
                    <div class="container text-center mt-3">
                        <h1 class="display-4">Hello {{auth()->user()->name}}</h1>
                        <p class="lead">Please scan QR Code customer to checkin</p>
                    </div>
                    <div class="d-flex justify-content-center">
                        <div id="qr-reader" style="width:500px"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <style>
        #preview{
            width:500px;
            height: 500px;
            margin:0px auto;
        }
    </style>
@endsection
@section('script')
    <script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
    <script src="{{asset('/js/html5-qrcode.min.js')}}"></script>
    <script src="{{asset('/js/sweetalert2.all.min.js')}}"></script>
    <script>
        var lastResult, countResults = 0;
        var isProcess = false;

        function docReady(fn) {
            // see if DOM is already available
            if (document.readyState === "complete"
                || document.readyState === "interactive") {
                // call on next available tick
                setTimeout(fn, 1);
            } else {
                document.addEventListener("DOMContentLoaded", fn);
            }
        }

        docReady(function () {
            function onScanSuccess(qrCodeMessage) {
                console.log('onScanSuccess : '+isProcess)
                if (!isProcess) {
                    isProcess = true;
                    getPersonalInfo(qrCodeMessage);
                }
            }

            var html5QrcodeScanner = new Html5QrcodeScanner(
                "qr-reader", { fps: 10, qrbox: 250 });
            html5QrcodeScanner.render(onScanSuccess);
        });
        function getPersonalInfo(content) {
            url = content+'/info';
            $.ajax({
                url : url,
                method: 'GET',
                success: function(response) {
                    submitForm(response);
                },
                error: function( json )
                {
                    isProcess = false;
                    if(json.status === 422) {
                        var errors = json.responseJSON;
                        $.each(json.responseJSON, function (key, value) {
                            alert(value)
                        });
                    }
                    if(json.status === 404) {
                        console.log(json.responseJSON);
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Data not found!',
                        })
                    }
                }
            });
            lastResult = null;
        }

        async function submitForm(content) {
            await Swal.fire({
                title: 'Masukkan Suhu Tubuh Pelanggan',
                html: '<table class="text-left">\n' +
                    '            <tr>\n' +
                    '                <th>Email</th>\n' +
                    '                <td>: '+content.email+'</td>\n' +
                    '            </tr>\n' +
                    '            <tr>\n' +
                    '                <th>Fullname</th>\n' +
                    '                <td>: '+content.name+'</td>\n' +
                    '            </tr>\n' +
                    '            <tr>\n' +
                    '                <th>IG Username</th>\n' +
                    '                <td>: '+content.ig_username+'</td>\n' +
                    '            </tr>\n' +
                    '            <tr>\n' +
                    '                <th>Phone Number</th>\n' +
                    '                <td>: '+content.mobile_num+'</td>\n' +
                    '            </tr>\n' +
                    '            <tr>\n' +
                    '                <th>Address</th>\n' +
                    '                <td>: '+content.address+'</td>\n' +
                    '            </tr>\n' +
                    '        </table>',
                input: 'text',
                inputPlaceholder : 'Input suhu tubuh',
                inputAttributes: {
                    autocapitalize: 'off',
                },
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                preConfirm: (suhu) => {
                    return fetch(content.url_code+'/checkin',{
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json, text-plain, */*",
                            "X-Requested-With": "XMLHttpRequest",
                            "X-CSRF-TOKEN": '{{csrf_token()}}'
                        },
                        method: 'post',
                        credentials : 'same-origin',
                        body: JSON.stringify({
                            'body-temp': suhu,
                            'num_code' : content.num_code,
                        },)
                    }).then(response => {
                            if (!response.ok) {
                                return response.text().then(text => {
                                    throw new Error(text)
                                });
                            }
                            return response.json()
                        })
                        .catch(error => {
                            Swal.showValidationMessage(error)

                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.isConfirmed) {
                    console.log(result.value.message);
                    Swal.fire({
                        icon : 'success',
                        title: result.value.message,
                    })
                }
            })
            isProcess = false;
            console.log('Submit Form : '+isProcess)
            lastResult = '';
        }
    </script>
@endsection
