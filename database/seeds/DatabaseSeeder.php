<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $user = new \App\User();
        $user->name = 'admin';
        $user->email = 'admin@mail.com';
        $user->password = bcrypt('admin');
        $user->address_id = 1;
        $user->save();

        $address = new \App\Address();
        $address->name_of_place = 'Rara';
        $address->save();
        $address = new \App\Address();
        $address->name_of_place = "Ruci's Joint";
        $address->save();
        $address = new \App\Address();
        $address->name_of_place = 'Vol.1';
        $address->save();
    }
}
