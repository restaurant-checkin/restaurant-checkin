<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (env('APP_ENV') === 'production') {
    URL::forceScheme('https');
}

Route::get('/', 'FormController@index');
Route::post('/store', 'FormController@store');
Route::get('/qr-code/{id}', 'FormController@show');
Route::get('/qr-code/{id}/checkin', function() {
    abort(403);
});

Route::get('/qr-code/{id}/checkin/info', 'FormController@info');

Route::post('/qr-code/{id}/checkin', 'FormController@update');

Route::get('/login', 'AuthController@showLoginForm')->name('login');
Route::post('/login', 'AuthController@login');
Route::get('/register', 'AuthController@showRegisterForm');
Route::post('/register', 'AuthController@register')->name('register');
Route::get('/logout', 'AuthController@logout');

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/export', 'FormController@showAllData')->name('export')->middleware('auth');
Route::post('/export/json', 'FormController@json')->name('export')->middleware('auth');
