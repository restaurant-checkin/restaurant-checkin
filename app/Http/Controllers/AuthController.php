<?php

namespace App\Http\Controllers;

use App\Address;
use App\User;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(){
        return view('auth.login');
    }

    public function login(Request $request){

        $validate = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if($validate->fails()) return redirect()->back()->withErrors($validate);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('home');
        }
        else{
            return redirect()->back()->with(['status' => 'Email or password invalid!']);
        }
    }

    public function showRegisterForm()
    {
        $address = Address::all();
        return view('auth.register', compact('address'));
    }

    public function register(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'email' => 'required|email|unique:users,email',
            'name' => 'required',
            'password' => 'required',
            'name_of_place' => 'required',
        ]);

        if($validate->fails()) return redirect()->back()->withErrors($validate)->withInput();

        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->address_id = $request->get('name_of_place');
        $user->save();

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('home');
        }
        else{
            return redirect()->back()->with(['status' => 'Email or password invalid!']);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

}
