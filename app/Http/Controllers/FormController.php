<?php

namespace App\Http\Controllers;

use App\Address;
use Cassandra\Custom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Validator;
use App\User;
use App\Visit;
use App\Customer;
use App\Notifications\SendQR;
use Notification;
use Datatable;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('form');
    }

    public function info($id)
    {
        $customer = Customer::where('num_code', '=', $id)->first();
        if(!$customer) abort(404);
        return response()->json($customer, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required',
            'fullname' => 'required',
            'ig_username' => 'required',
            'area' => 'required',
            'phone' => 'required|numeric',
            'transportation' => 'required',
            'travel' => 'required',
        ]);

        $isUser = Customer::where('email', '=', $request->get('email'))->exists();

        if($isUser) return redirect()->back()
            ->withErrors(['email'=>
                'Email yang anda masukan telah digunakan, silahkan gunakan QR yang telah berhasil dibuat sebelumnya!'])->withInput();

        if ($validate->fails()) return redirect()->back()->withErrors($validate)->withInput();

        $uniqid = md5(uniqid(rand(), true));
        $customer = new Customer();
        $customer->email = $request->get('email');
        $customer->name = $request->get('fullname');
        $customer->ig_username = $request->get('ig_username');
        $customer->address = $request->get('area');
        $customer->mobile_num = $request->get('phone');
        $customer->transportation = implode(",", $request->get('transportation'));
        $customer->another_city = ($request->get('travel') == 'other' ? $request->get('other-travel-text') : $request->get('travel'));
        $customer->url_code = url('/qr-code/'.$uniqid);
        $customer->num_code = $uniqid;

        $customer->notify(new SendQR($customer->url_code));
        $customer->save();

        return redirect(url('/qr-code/'.$uniqid));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::where('num_code', '=',$id)->first();
        if($customer == null){
            abort(404);
        }
        return view('show-qr-code', compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::where('num_code', '=', $id)->first();
        if($customer == null) abort(404);
        return view('checkin', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(auth()->user() == null) abort(403);

        $validate = Validator::make($request->all(),[
            'body-temp' => 'required|numeric'
        ]);

        if($validate->fails()) return response()->json('Input suhu tubuh harus decimal', 422);

        $customer = Customer::where('num_code', '=', $id)->first();

        $now = Carbon::now();

        $visit = new Visit();
        $visit->customer_id = $customer->id;
        $visit->timevisit = $now->format('H:i:s');
        $visit->datevisit = $now->format('Y-m-d');
        $visit->user_id = auth()->user()->id;
        $visit->body_temperature = $request->get('body-temp');
        $visit->save();

        return response()->json([
            'message' => 'Berhasil memasukkan data!',
            'status' => 201], 201);
    }

    public function showAllData()
    {
        return view('export');
    }

    public function json(Request $request)
    {
        $filter_date = $request->get('filter_date');
        $start_date = Carbon::now()->format('Y-m-d');
        $end_date = null;

        if ($filter_date == '2-weeks') {
            $end_date = Carbon::now()->subDays(14)->format('Y-m-d');
        } else if ($filter_date == '1-month') {
            $end_date = Carbon::now()->subDays(30)->format('Y-m-d');
        } else if ($filter_date == '3-month') {
            $end_date = Carbon::now()->subDays(90)->format('Y-m-d');
        } else if ($filter_date == '6-month') {
            $end_date = Carbon::now()->subDays(180)->format('Y-m-d');
        } else {
            $end_date = Carbon::now()->subDays(365)->format('Y-m-d');
        }

        $visit = Visit::select(
            'visits.*',
            'customers.name as customer_name',
            'customers.email as customer_email',
            'customers.ig_username as customer_ig_username',
            'customers.address as customer_address',
            'customers.mobile_num as customer_mobile_num',
            'customers.transportation as customer_transportation',
            'customers.another_city as customer_another_city',
            'customers.num_code as customer_num_code',
            'customers.created_at as customer_created_at',
            'users.email as users_email',
            'users.name as users_name',
            'addresses.name_of_place as users_name_of_place',
            'users.created_at as users_created_at'
        )->leftJoin('users', 'users.id', '=', 'visits.user_id')
        ->leftJoin('customers', 'customers.id', '=', 'visits.customer_id')
        ->leftJoin('addresses', 'addresses.id', '=', 'users.address_id')
        ->whereNotNull('customers.email')
        ->whereNotNull('users.email')
        ->whereBetween('visits.datevisit', [$end_date, $start_date])->get();
        return response()->json($visit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
